import { useContext } from "react";
import { NavLink, useLocation, useNavigate } from "react-router-dom"
import { AuthContext } from "../../context";
import { linkNav } from "../../utils"

const { publicLink, adminLink } = linkNav;

export const Navbar = () => {

    const { auth } = useContext(AuthContext);

    const navigate = useNavigate();
    const {pathname} = useLocation();

    const handleNavigate = ( url: string) => {
        navigate(url)
    }

  return (
        <nav className="navbar"> 

            <h1>Navigation</h1>
            {/* <img src="/assets/react.svg" alt="" /> */}

            <ul>
            <span>NavLink</span>
                {
                    publicLink.map( d => (
                        <li
                            key={d.url}
                        >
                            <NavLink
                                to={d.url}
                                className={({isActive})=> isActive?'nav-active': '' }
                            >
                                {d.name}
                            </NavLink>
                        </li>
                    ))
                }
               
            </ul>

            <span>Use Navigate</span>
            <div className="navigation">

                {
                    publicLink.map( d => (
                        <div 
                            className="links"
                            key={d.url}
                        >
                            <div 
                                className={ pathname === d.url ? 'item-active': 'item'}
                                // className="item"
                                onClick={() =>handleNavigate(d.url)}
                            >
                                {/* <NavLink
                                    to={d.url}
                                    className={({isActive})=> isActive?'item-active': '' }
                                >
                                </NavLink> */}
                                    <span>
                                        {d.name}
                                    </span>
                            </div>
                            
                        </div>
                    ))
                }

                {
                    auth && adminLink.map( d => (
                        <div 
                            className="links"
                            key={d.url}
                        >
                            <div 
                                className={ pathname === d.url ? 'item-active': 'item'}
                                onClick={() =>handleNavigate(d.url)}
                            >
                                    <span>
                                        {d.name}
                                    </span>
                            </div>

                        </div>
                    ))
                }
            </div>
        </nav>
  )
}
