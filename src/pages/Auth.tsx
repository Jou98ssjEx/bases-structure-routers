import React, { FC, useContext } from 'react'
import { MainLayout } from '../components'
import { AuthContext } from '../context'
import { IPropsPages } from '../interfaces'

export const AuthPage:FC<IPropsPages> = ({title}) => {

  const { singIn, auth } = useContext(AuthContext);
  
  return (
    <MainLayout
        title={title}
    >

      {
        auth 
            ? ('Estas Authenticado')
            : (
              <div className="text-center">
                  <button
                    onClick={singIn}
                    className='mt-4 btn btn-outline-success w-25'
                  >
                      Auth
                  </button>
              </div>

            )
      }


    </MainLayout>
  )
}
