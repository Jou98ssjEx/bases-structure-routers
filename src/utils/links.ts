import { Ilinks } from "../interfaces";


export const linkNav: Ilinks = {
    publicLink:[
        {
            name: 'Home',
            title: 'Home Page',
            url: '/',
        },
        {
            name: 'About',
            title: 'About Page',
            url: '/about',
        },
        {
            name: 'Contact',
            title: 'Contact Page',
            url: '/contact',
        },
        // {
        //     name: 'Auth-Login',
        //     title: 'Auth-Login Page',
        //     url: '/login',
        // },
        {
            name: 'Auth',
            title: 'Auth Page',
            url: '/auth',
        },
    ],

    adminLink: [
        {
            name: 'Admin',
            title: 'Admin Page',
            url: '/admin',
        },
        {
            name: 'Admin User',
            title: 'Admin User Page',
            url: '/users',
        },
        // {
        //     name: 'Logout',
        //     title: 'Logout Page',
        //     url: '/logout',
        // },
    ]
}