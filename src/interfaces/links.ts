

export interface Ilinks {
    adminLink: IAdminLinks[];
    publicLink: IPublicLinks[];
}

export interface IAdminLinks {
    name: string,
    title: string,
    url: string,
}
export interface IPublicLinks {
    name: string,
    title: string,
    url: string,
}