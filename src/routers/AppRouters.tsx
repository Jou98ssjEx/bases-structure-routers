import { useContext } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom"
import { Header, Navbar } from "../components"
import { AuthContext } from "../context";
import { About, AuthPage, Contact, Home, AdminPage, UserPage } from "../pages"
import { linkNav } from "../utils"

const { publicLink, adminLink } = linkNav;
const [ admin, user ] = adminLink;
const [ home, about, contact, auth ] = publicLink;

export const AppRouters = () => {

  const { auth: logged } = useContext(AuthContext);
  return (
    <BrowserRouter>
      <Header />
      <div className="main-layout">
        <Navbar />

          <Routes>

              <Route path={home.url} element={ <Home title={home.title} /> } />
              <Route path={about.url} element={<About title={about.title} />} />
              <Route path={contact.url} element={<Contact title={contact.title} /> } />
              <Route path={auth.url} element={<AuthPage title={auth.title} /> } />

              {
                logged && (
                    <>
                      <Route path={admin.url} element={<AdminPage  /> } />
                      <Route path={user.url} element={<UserPage  /> } />
                    </>
                  )
                }



              <Route path="/*" element={ <Navigate to='/' replace /> } />
          </Routes>
      </div>
    </BrowserRouter>
  )
}
