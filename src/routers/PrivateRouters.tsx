import { FC, useContext } from "react"
import { Navigate } from "react-router-dom";
import { AuthContext } from "../context"
import { IAppRouter } from "../interfaces"

export const PrivateRouters: FC<IAppRouter> = ({children}) => {

    const { auth } = useContext(AuthContext);

  return auth ? <Navigate to={'/'} /> : <> {children} </>
}
