import { createContext } from 'react';

interface ContextProps {
    auth: boolean;

    // methods
    singIn: () => void
    logout: () => void
}

 export const AuthContext = createContext({} as ContextProps);