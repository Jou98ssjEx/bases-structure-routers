import { AuthState } from './';

type AuthActionType = 
   | { type: '[Auth] - SingIn'}
   | { type: '[Auth] - Logout'}

export const authReducer  = ( state: AuthState, action: AuthActionType ): AuthState => {
   switch (action.type) {

       case '[Auth] - SingIn':
           return {
               ...state,
                auth: true
           }         

        case '[Auth] - Logout':
            return{
                ...state,
                auth: false
            }

       default:
          return state;
   }
} 