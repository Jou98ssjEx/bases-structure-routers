import { AuthProvider } from "./context"
import { AppRouters } from "./routers"

export const App = () => {
  return (
    <AuthProvider>
      <AppRouters />
    </AuthProvider>
  )
}
